<?php
//ini_set('date.timezone', 'America/New_York');

$config['base_url'] = 'http://leadgeneral.dev/';// Base URL including trailing slash (e.g. http://localhost/)
$config['stat']     = $config['base_url']."static/";
$config['logo_w']   = $config['stat']."img/logo_w.png";
$config['logo_h']   = $config['stat']."img/logo_h.png";

$config['default_controller'] = 'user';// Default controller to load
$config['error_controller']   = 'error';// Controller used for errors (e.g. 404, 500 etc)

$config['db_host']     = 'localhost';// Database host (e.g. localhost)
$config['db_name']     = 'leadgen';// Database name
$config['db_username'] = 'root';// Database username
//$config['db_username'] = '4923KtSqcB6zp';// Database username
$config['db_password'] = 'mysql';// Database password //password

?>