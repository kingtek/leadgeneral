<?php

/**
 * Class StringColumn is a child column class used
 * to describe columns with string content
 *
 * @author Alexander Gilmanov
 *
 * @since May 2012
 */

class EmailColumn extends Column {
	
    protected $_jsDataType = 'html';
    protected $_dataType = 'string';
        
    
    public function __construct( $params = array () ) {
		parent::__construct( $params );
		$this->_dataType = 'email';
    }
    
    public function formatHandler( $cell ) {
		if(!is_array($cell->getContent())){
			return '<span><a href="mailto:'.$cell->getContent().'">'.$cell->getContent().'</a></span>';
		}else{
			$value = $cell->getContent();
			return '<span><a href="mailto:'.$value['value'].'">'.$value['value'].'</a></span>';
		}
    }    
    
}


?>
