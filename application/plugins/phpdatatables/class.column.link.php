<?php

/**
 * Class LinkColumn is a child column class used
 * to describe columns which have links in the cells
 *
 * @author Alexander Gilmanov
 *
 * @since July 2012
 */

class LinkColumn extends Column {
	
    protected $_jsDataType = 'string';
    protected $_dataType = 'string';
    
    public function __construct( $params = array () ) {
		parent::__construct( $params );
		$this->_dataType = 'link';
    }
    
    public function formatHandler( $cell ) {
		if(!is_array($cell->getContent())){
			return '<span><a href="'.$cell->getContent().'">'.$cell->getContent().'</a></span>';
		}else{
			$class = $cell->getAdditionalParameter('class');
			if($class != ''){
				$class = 'class="'.$class.'"';
			}
			return '<span><a '.$class.' href="'.$cell->getAdditionalParameter('link').'">'.$cell->getContent().'</a></span>';
		}
    }    
    
}


?>
