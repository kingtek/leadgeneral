<?php

/**
 * Class LinkColumn is a child column class used
 * to describe columns which have links in the cells
 *
 * @author Alexander Gilmanov
 *
 * @since July 2012
 */

class IconColumn extends Column {
	
    protected $_jsDataType = 'string';
    protected $_dataType = 'string';
    
    public function __construct( $params = array () ) {
		parent::__construct( $params );
		$this->_dataType = 'icon';
    }
    
    public function formatHandler( $cell ) {
		return '<i class="icon-'.$cell->getAdditionalParameter('icon').'" title="'.$cell->getContent().'"></i>';
    }    
    
}


?>
