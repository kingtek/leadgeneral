<?php
/**
* Settings file for the phpDataTables module
* 
* @author Alexander Gilmanov cjbug@ya.ru
* @since 10.10.2012
*
**/


/**
 * Path settings.
 * Paths are relative by default, but you may change it as you wish
 */
define('PDT_ROOT_PATH', dirname(__FILE__)); // full path to the phpDataTables root directory
define('PDT_ROOT_URL', ''); // full URL of phpDataTables module
define('PDT_TEMPLATE_PATH', PDT_ROOT_PATH.'/templates/'); // path to phpDataTables templates. You should not change this setting if you use default templates
define('PDT_ASSETS_PATH', STAT.'assets/'); // path to phpDataTables assets directory. You should not change this setting if you don't change default CSS/JS
define('PDT_CSS_PATH', STAT.'assets/css/'); // path to phpDataTables CSS styles. You should not change this setting if you use default CSS
define('PDT_JS_PATH', STAT.'assets/js/'); // path to phpDataTables javascript. You should not change this setting if you use default javascripts.


/**
 * Settings which define whether we include the JS files
 * from the module build or not
 * (if user already has them included in the page)
 */ 
define('PDT_INCLUDE_JQUERY_CORE', true); // Whether to include link to jQuery javascript to the generated page. Set to false if you already have jQuery (1.8 or higher) included in your project.
define('PDT_INCLUDE_DATATABLES_CORE', true); // Whether to include link to jQuery DataTables plugin javascript to the generated page. Set to false if you already have DataTables included in your project (version used in phpDataTables is 1.9.1, newer version will be provided with updates).

/**
 * MySQL settings for query-based tables
 */ 
define('PDT_ENABLE_MYSQL', false); // Whether to use MySQL in phpDataTables. Disable if you are not going to access MySQL directly from phpDataTables.
define('PDT_MYSQL_HOST', 'localhost'); // Name or address of MySQL host
define('PDT_MYSQL_DB', 'leadgen'); // Name of MySQL database to use
define('PDT_MYSQL_USER', 'root'); // Name of MySQL user
define('PDT_MYSQL_PASSWORD', 'mysql'); // Password to use in MySQL

/**
 * Memory settings 
 */
ini_set('memory_limit','64M'); // disable this if you arent't going to work with big datasets
define('PDT_ENABLE_MEMCACHE', false); // enable memcache (if installed on server)
define('PDT_MEMCACHE_HOST','127.0.0.1'); // Name or address of Memcache host
define('PDT_MEMCACHE_PORT','11211'); // Memcache TCP port
define('PDT_MEMCACHE_TIME', 86400) // how long do we store in cache in seconds (default 24 hours)

?>
