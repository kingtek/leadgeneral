<?php

class Error extends Controller {
	
	function index()
	{
		$this->code("404");
	}
	

	/**
	 *  Displays a error message based off of pre-set error codes.
	 */

	function code($code)
	{

		switch ($code) {
			case '404':
				echo "NOT FOUND";
				break;

			case '403':
				echo "ACCESS DENIED!"; //invalid permissions error
				break;
			
			default:
				# code...
				break;
		}

	}
    
}

?>
