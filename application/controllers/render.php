<?php

//layout helper
$layout = $this->loadView($layout);

//get the client's information from the database by url //static for now
//$clientId = 1;
//$clientData = $this->db->read("*","client","client_id = '$clientId'");
//$pageVar['logo'] = "<img src='".$clientData[0]->logo_h."'>";
//$pageVar['header_logo'] = "<img src='".$clientData[0]->logo_w."' style='max-width:200px;'>";


//adds automatic support for the flash messages
if(!empty($flashMessage)){
$pageVar['flashMessage'] = $flashMessage;
}



if(isset($user)){
$pageVar['user'] = $user;
}


if(isset($page_nav)){
$pageVar['page_nav'] = $page_nav;
}else{
	$pageVar['page_nav'] = array(); //prevents sidebar errors;
}

//loop and set each object
if(!empty($pageVar)){
foreach ($pageVar as $key => $value) {
    $layout->set($key, $value);
}
}


//render the layout;
$layout->render();
?>