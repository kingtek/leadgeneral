<?php

class Lists extends Controller {
	public function __construct() {
		require ('db.php');

		$this->db = $db;
	}

	public function index() {
		require_once ('init.php');
		$layout = 'layout_1';

		$pageVar['title'] = 'Lists';

		$mylists = $this->db->read('*', 'lists', "user_id = '$user->user_id'");

		$body = "<a href='/lists/create' class='btn btn-default'>Create List</a> <a href='/lists/export' class='btn btn-default'>Export Lists</a> <a href='/lists/filter' class='btn btn-default'>Apply Filter</a>";
		if ($mylists != false) {
			$body .= '<h3>Your List</h3>';
			$body .= "<table id='tablecloth' class='display' cellspacing='0' width='100%'>
        <thead>
            <tr>
                <th>List Name</th>
                <th>Lead Count</th>

            </tr>
        </thead>
         <tbody>";
			foreach ($mylists as $mylist) {
				//get lists leads count
				$count = $this->db->read('lead_list_id', 'lead_list', "lists_id = '$mylist->lists_id'");
				if ($count != false) {
					$count = count($count);
				} else {
					$count = 0;
				}

				$body .= "
         	<tr>
                <td><a href='/lists/manage/$mylist->lists_id'>$mylist->name</a></td>
                <td>$count</td>
            </tr>";
			}

			$body .= "
		 </tbody>
		 </table>";
		} else {
			$body .= "<h3>As you create list, they will show up here.</h3>";
		}

		$pageVar['body'] = $body;

		require_once ('render.php');
	}

	public function create() {
		require_once ('init.php');

		if ($_POST) {
			$_POST['user_id'] = $user->user_id;
			$create           = $this->db->create('lists', $_POST);
			if ($create != false) {
				$_SESSION['flashMessage'] = array('success', '', 'New list created!');
			} else {
				$_SESSION['flashMessage'] = array('danger', '', 'Unable to create list at this time.');
			}
			$this->redirect('lists');

		} else {
			$layout = 'layout_1';

			$pageVar['title'] = 'Create A List';

			$body = "
							 <form role ='form' action ='/lists/create' method='post' >
							 <div class='form-group' >
							 <label> List Name </label >
							 <input class  = 'form-control required' name='name' >
							 <p class ='help-block' >  </p>
							 </div>

							 <div class='form-group'>
							 <label > Describe this List(reccomended, but not required) </label >
							 <textarea class='form-control' rows='3' name='description'></textarea >
							 </div >

							  <div class='form-group'>
							 <label >Smart List</label >
							 <input type='checkbox' name='smart_list'/>
							 <p>Smart List adds new leads as they become available in the system. View more abut smart lists <a href=''>here</a>.</p>
							 </div >

							 <button type ='submit' class ='btn btn-primary' > Create List </button >
							 </form >
							";

			$pageVar['body'] = $body;
			require_once ('render.php');
		}

	}

}

?>