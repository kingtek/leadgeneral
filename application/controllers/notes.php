<?php

class Notes extends Controller {
	public function __construct() {
		require ('db.php');

		$this->db = $db;
	}

	public function index() {
		require_once ('init.php');
		$layout = 'layout_1';

		$pageVar['title'] = 'Notes';

		//10 most recent queries
		//10 most recent contacts accessed
		//10 most recent lists created
		//recent notes
		$body = "<div class='row'>
                <div class='col-lg-8'>
                    <div class='panel panel-default'>
                        <div class='panel-heading'>
                            <i class='fa fa-leaf fa-fw'></i>Notes Widget

                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div id='morris-area-chart'></div>
                        </div>
                        <!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>
                </div>";

		$pageVar['body'] = $body;

		require_once ('render.php');
	}

}

?>