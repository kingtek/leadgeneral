<?php

class Contact extends Controller {

	public function __construct() {
		require ('db.php');

		$this->db           = $db;
		$this->googleSearch = "https://plus.google.com/local/";
	}

	public function index() {
		require_once ('init.php');

		//get all contacts in the system
		$contacts = $this->db->read("*", 'contact', 'contact_id > 0 ORDER BY contact_id DESC LIMIT 0,100');
		if ($contacts != false) {
			$layout = 'main_view';
			foreach ($contacts as $contact) {
				$contact->contact_id = "<a href='contact/info/$contact->contact_id'>Manage Lead</a>";
				$contactArray[]      = get_object_vars($contact);
			}
			//require_once ('render.php');
			$tbl = new PHPDataTable();
			$tbl->buildByArray($contactArray);

			//$tbl->enableAdvancedFilter();
			// fixing the header and 3 first columns
			$tbl->fixColumns(4);
			// setting the top offset to fit the main menu strip
			$tbl->setTopOffset(10);
			// setting the number of display rows to 100 for the table to be long
			$tbl->setDisplayLength(10);

			//$tbl->enableAdvancedFilter();

			$pageVar['tbl'] = $tbl;

		} else {
			$layout          = 'form_view';
			$page["form"]    = "<div class='container'><h3>Generate Some Leads To Get Started</h3></div>";
			$pageVar['page'] = $page;
		}

		require_once ('render.php');
	}

	public function test() {
		require_once ('init.php');
		$layout = 'blank_view';

		require_once ('render.php');
	}

	public function index1() {
		require_once ('init.php');
		$layout = 'main_view';

		$page["form"] = "
		<table id='example' class='display' cellspacing='0' width='100%'>
        <thead>
            <tr>
                <th>Business Name</th>
                <th>Website</th>
                <th>Industry</th>
                <th>City</th>
                <th>State</th>
            </tr>
        </thead>";
		$contacts = $this->db->read("*", 'contact', 'contact_id > 0');
		foreach ($contacts as $contact) {
			$contact_id = $contact->contact_id;

			$business_name     = "<a href='/contact/info/$contact_id'>" .$contact->business_name."</a>";
			$business_phone    = "<a href='/contact/info/$contact_id'>" .$contact->business_phone."</a>";
			$business_web_url  = "<a href='/contact/info/$contact_id'>" .$contact->business_web_url."</a>";
			$business_email    = "<a href='/contact/info/$contact_id'>" .$contact->business_email."</a>";
			$business_industry = "<a href='/contact/info/$contact_id'>" .$contact->industry."</a>";
			$business_city     = "<a href='/contact/info/$contact_id'>" .$contact->business_city."</a>";
			$business_state    = "<a href='/contact/info/$contact_id'>" .$contact->business_state."</a>";

			$page["form"] .= "<tr><td>$business_name</td><td>$business_web_url</td><td>$business_industry</td><td>$business_city</td><td>$business_state</td></tr>";
		}

		$page["form"] .= "<tbody></tbody></table>";

		$pageVar['page'] = $page;

		require ('render.php');
	}

	function buildUrl() {
		if ($_POST) {

			$industry = str_replace(" ", "+", (strtolower($_POST['industry'])));

			$city  = strtolower($_POST['location_city']);
			$state = strtolower($_POST['location_state']);

			$_SESSION['city']  = $city;
			$_SESSION['state'] = $state;

		}
		$this->redirect("contact/generateLeads/$industry/$city/$state");
	}

	function generate() {

		require_once ('init.php');
		$layout       = 'form_view';
		$page["form"] = "<h2>Generate Leads</h2> <form action='/contact/buildUrl' method='post'enctype='multipart/form-data'>
			<table style='width:100%' >
			<tr height='100'><td>Industry</td><td><input type='text' name='industry'></td></tr>
			<tr height='100'><td>Location City</td><td><input type='text' name='location_city'></td></tr>
			<tr height='100'><td>Location State</td><td><select name='location_state'>
            	<option value='GA'>Georgia</option>
            	<option value='AL'>Alabama</option>
            	<option value='AK'>Alaska</option>
            	<option value='AZ'>Arizona</option>
            	<option value='AR'>Arkansas</option>
            	<option value='CA'>California</option>
            	<option value='CO'>Colorado</option>
            	<option value='CT'>Connecticut</option>
            	<option value='DE'>Delaware</option>
            	<option value='DC'>District Of Columbia</option>
            	<option value='FL'>Florida</option>
            	<option value='GA'>Georgia</option>
            	<option value='HI'>Hawaii</option>
            	<option value='ID'>Idaho</option>
            	<option value='IL'>Illinois</option>
            	<option value='IN'>Indiana</option>
            	<option value='IA'>Iowa</option>
            	<option value='KS'>Kansas</option>
            	<option value='KY'>Kentucky</option>
            	<option value='LA'>Louisiana</option>
            	<option value='ME'>Maine</option>
            	<option value='MD'>Maryland</option>
            	<option value='MA'>Massachusetts</option>
            	<option value='MI'>Michigan</option>
            	<option value='MN'>Minnesota</option>
            	<option value='MS'>Mississippi</option>
            	<option value='MO'>Missouri</option>
            	<option value='MT'>Montana</option>
            	<option value='NE'>Nebraska</option>
            	<option value='NV'>Nevada</option>
            	<option value='NH'>New Hampshire</option>
            	<option value='NJ'>New Jersey</option>
            	<option value='NM'>New Mexico</option>
            	<option value='NY'>New York</option>
            	<option value='NC'>North Carolina</option>
            	<option value='ND'>North Dakota</option>
            	<option value='OH'>Ohio</option>
            	<option value='OK'>Oklahoma</option>
            	<option value='OR'>Oregon</option>
            	<option value='PA'>Pennsylvania</option>
            	<option value='RI'>Rhode Island</option>
            	<option value='SC'>South Carolina</option>
            	<option value='SD'>South Dakota</option>
            	<option value='TN'>Tennessee</option>
            	<option value='TX'>Texas</option>
            	<option value='UT'>Utah</option>
            	<option value='VT'>Vermont</option>
            	<option value='VA'>Virginia</option>
            	<option value='WA'>Washington</option>
            	<option value='WV'>West Virginia</option>
            	<option value='WI'>Wisconsin</option>
            	<option value='WY'>Wyoming</option>
            </select></td></tr>
			</td></tr>
			<tr><td></td><td><input type='submit' value='Generate Contacts'></td></tr>
			</table>
			</form>";
		$pageVar["page"] = $page;
		require_once ('render.php');
	}

	public function generateLeads($industry, $location_city, $location_state) {
		require_once ('init.php');
		require ('application/plugins/simple_html_dom.php');
		$hash             = md5(time());
		$_SESSION['hash'] = $hash;

		$industry = urlencode(strtolower($industry));

		$location = $location_city." ".$location_state;

		$locationEncoded = rawurlencode($location);

		$parseUrl = $this->googleSearch.$locationEncoded."/s/".$industry;
		//echo "<h2>basic parse of $parseUrl</h2><hr/>";
		$html = file_get_html($parseUrl);

		foreach ($html->find('h3') as $contact) {

			//echo $contact->plaintext;
			foreach ($contact->find('a') as $link) {

				$link = explode('.', $link->href);

				//dan getting google id
				$google_id = str_replace('/about', '', $link['1']);
				$google_id = str_replace('/', '', $google_id);

				$link = "https://plus.google.com".$link['1'];

				//create the contact based off of link
				$contact = $this->db->create("contact", array("google_plus_link" => $link, 'hash' => $hash, 'google_id' => $google_id, 'user_id' => $user_id));

				if ($contact != false) {
					//we have successfully created a tmp record
					$contact_id = $contact['id'];
					//id to refrence update commands

					$html = file_get_html($link);

					//Get the Company Name and make sure it only gets set once.
					foreach ($html->find('div[class=rna]') as $CompanyName) {
						if (!isset($Company)) {

							$Company = $CompanyName->plaintext;
							// echo "<b>Company</b>: <b>" . $Company . "</b>";

							$this->db->update('contact', array("business_name" => $Company), array("contact_id", $contact_id));
						}
					}

					unset($Company);

					//echo $link . "";

					//Checking to see if Verified. If not then it is declared after the foreach loop.
					foreach ($html->find('div[class=xR]') as $verified) {
						//Verified  or Not
						if (!isset($ConfirmVerifiedPage)) {
							$ConfirmVerifiedPage = $verified->plaintext;
							if ($ConfirmVerifiedPage != "") {
								//  echo "<b><font color='006600'>" . $ConfirmVerifiedPage . "</font></b>";
								$isVerified = "YES";
							}

						}
					}

                    //Set Email if it exist
                    $business_email = null;
                    $returnValue = preg_match('/([^[^[^"^@\\s]+)@((?:[-a-z0-9]+\\.)+[a-z]{2,})/', $html, $business_email);
                    $business_email = $business_email['0'];
                    $this->db->update('contact', array("business_email" => $business_email), array("contact_id", $contact_id));

					//If it is not Verified set it here
					if ($ConfirmVerifiedPage == "") {
						//echo "<font color='ff0000'><b>Not Verified</b></font>";
						$isVerified = "NO";
					}
					$this->db->update('contact', array("is_verified" => $isVerified), array("contact_id", $contact_id));
					unset($ConfirmVerifiedPage);

					//Get Address | Number | URL | Category | Hours
					$CompanyInfoCount = 1;
					foreach ($html->find('div[class=Ny]') as $CompanyInfo) {
						if ($CompanyInfoCount == 1) {
							$Address = $CompanyInfo->plaintext;
							//echo "<b>Address</b>: " . $Address . "";
							//$masterArray['business_address'] = $Address;
							$this->db->update('contact', array("business_address" => $Address), array("contact_id", $contact_id));
						}
						if ($CompanyInfoCount == 2) {
							$Number = $CompanyInfo->plaintext;
							//echo "<b>Number</b>: " . $Number . "";
							//$masterArray['business_phone'] = $Number;
							$this->db->update('contact', array("business_phone" => $Number), array("contact_id", $contact_id));
						}
						if ($CompanyInfoCount == 3) {
							$URL = $CompanyInfo->plaintext;
                            //Set Email if it exist
                            $returnValue = preg_match('/([\.])/', $URL, $UrlCheck);
                            $UrlCheck = $UrlCheck['0'];

                            if($UrlCheck != ""){$URL = $URL;}
                            else{$URL = ""; $CompanyInfoCount = 4;}
							// echo "<b>URL</b>: " . $URL . "";
							//$masterArray['business_web_url'] = $URL;
							$this->db->update('contact', array("business_web_url" => $URL), array("contact_id", $contact_id));
						}
						if ($CompanyInfoCount == 4) {
							$Category = $CompanyInfo->plaintext;
							//echo "<b>Main Category</b>: " . $Category . "";
							//$masterArray['business_category'] = $Category;
							$Category = str_replace("$", '', $Category);
							$this->db->update('contact', array("business_category" => $Category), array("contact_id", $contact_id));
						}
						if ($CompanyInfoCount == 5) {
							$Hours = $CompanyInfo->plaintext;
							// echo "<b>Hours</b>: " . $Hours . "";
							//$masterArray['business_hours'] = $Hours;
							$this->db->update('contact', array("business_hours" => $Hours), array("contact_id", $contact_id));
						}
						$CompanyInfoCount++;

						//echo $CompanyInfo->plaintext."";

					}

					// echo "<b>Other Categories</b>";

					foreach ($html->find('div[class=s9] span[class=d-s]') as $Categories) {
						$tag                = $Categories->plaintext;
						$_SESSION['tags'][] = $tag;

					}
					//$tags = implode(',', $html->find('div[class=s9] span[class=d-s]'));//comma join these as tags to use later
					//possibly create a tag table, and just refrence tag by id and / or create new tags for more matching options later
					//$masterArray['business_tags'] = $tags;

					//echo " <b>Views</b>";

					//Show Profile Views & Followers. Only need to see the number once.
					//If there are no followers it only shows views. Uses Pipe to separate
					foreach ($html->find('div[class=Zmjtc]') as $CompanyViews) {
						if (!isset($Views)) {
							$Views = $CompanyViews->plaintext;
							if ($Views < 1) {
								$Views = 0;
							}
							str_replace('views', "", $Views);
							$this->db->update('contact', array("business_views" => $Views), array("contact_id", $contact_id));
							unset($Views);

							//echo $Views . "";
						}
					}

					//$masterArray['business_views'] = $Views;
				}
				// echo "<hr align='left' width='50%'>";
			}

			//update record with masterArray
			//$this->db->update('contact', $masterArray, array("contact_id", $contact_id));
		}

		//echo $html->plaintext;

		//Dustin can do the parsing magic from here now that everything is in place
		$this->redirect('contact/searchResult');
	}

	public function info($contact_id) {
		require_once ('init.php');
		$layout  = 'contact_view';
		$contact = $this->db->read('*', 'contact', "contact_id = '$contact_id'");

		if ($contact != false) {
			$contact = $contact[0];
		}

		$page["form"] = "<h3>$contact->business_name <span style='font-size:15px;'><!--<a href='#' class='btn btn-sm btn-default'><i class='fa fa-pencil'></i> Edit</a></span>--></h3><form action='/contact/setStatus/$contact_id' method='post'>
		<select name='status'>
		<option value='cold'>Cold</option>
		<option value='warm'>Warm</option>
		<option value='hot'>Hot</option>
		</select>
		<input type='submit' value='Update Status' />


		</form><br/>";

		if ($contact->status == 'cold') {
			$page["form"] .= "<p class='label label-info' style='font-size:17px;'>Current Status: $contact->status </p>";
		}

		if ($contact->status == 'warm') {
			$page["form"] .= "<p class='label label-warning' style='font-size:17px;'>Current Status: $contact->status </p>";
		}

		if ($contact->status == 'hot') {
			$page["form"] .= "<p class='label label-danger' style='font-size:17px;'>Current Status: $contact->status </p>";
		}

		$page["form"] .= "
		<hr/>
		<table id='' class='table table-bordered' cellspacing='0' width='100%'>
        <thead>
            <tr>
            	 <th>Key</th>
                <th>Value</th>
            </tr>
        </thead>";

		foreach ($contact as $key => $value) {
			if ($key != 'contact_id') {

				$page["form"] .= "<tr><td>$key</td><td>$value</td></tr>";
			}
		}
		$page["form"] .= "<tbody></tbody></table>";

		$pageVar['contact_id'] = $contact_id;//pass the contact_id to the view
		//get the notes for the contact

		$notes = $this->db->read('*', 'note', "contact_id = '$contact_id ORDER BY date_added ASC'");
		if ($notes != false) {
			$page["note"] = "<table id='note' class='table table-bordered' cellspacing='0' width='100%'>
        <thead>
            <tr>
            	 <th>Note</th>
            	 <th>Rep</th>
                <th>Date</th>
            </tr>
        </thead>";

			foreach ($notes as $note) {

				$noteuser = $this->db->read('name', 'user', "user_id = '$note->user_id'");
				if ($noteuser != false) {
					$noteuser = $noteuser[0];
				} else {
					$noteuser->name = 'NA';
				}$page["note"] .= "<tr><td>$note->message</td><td>$noteuser->name</td><td>$note->date_added</td></tr>";
			}
			$page["note"] .= "<tbody></tbody></table>";
		} else {
			$page["note"] = "<p>No Notes Yet.</p>";
		}

		$pageVar["page"] = $page;
		require_once ('render.php');
	}

	public function searchResult1() {
		require_once ('init.php');
		$hash                = $_SESSION['hash'];
		$contacts            = $this->db->read('*', 'contact', "hash = '$hash'");
		$layout              = 'table_view';
		$pageVar['contacts'] = $contacts;

		require_once ('render.php');
	}

	public function searchResult() {
		require_once ('init.php');
		$hash     = $_SESSION['hash'];
		$contacts = $this->db->read('*', 'contact', "hash = '$hash'");
		$layout   = 'data_table_view';
		//$pageVar['contacts'] = $contacts;
		foreach ($contacts as $contact) {
			$contactArray[] = get_object_vars($contact);
		}

		//require_once ('render.php');
		$tbl = new PHPDataTable();
		$tbl->buildByArray($contactArray);
		$tbl->enableAdvancedFilter();

		$pageVar['tbl'] = $tbl;
		require_once ('render.php');
	}

	public function saveNote($contact_id) {
		if ($_POST) {
			require_once ('init.php');
			$date       = date("m/d/Y", time());
			$time       = date("h:i a", time());
			$time_stamp = $date." at ".$time;

			$note = $this->db->create('note', array("message" => $_POST['message'], "user_id" => $user_id, "date_added" => $time_stamp, "contact_id" => $contact_id));
			if ($note != false) {
				$_SESSION['flashMessage'] = array("success", "", "Note Saved!");
			} else {
				$_SESSION['flashMessage'] = array("danger", "", "Unable to save note at this time!");
			}
			$this->redirect("contact/info/$contact_id");
		}
	}

	public function clearSearchHistory() {
		$_SESSION['tags'] = '';
		$this->redirect('contact/searchResult');
	}

	public function purge() {
		//select all contacts
		$contacts   = $this->db->read("contact_id,google_id", 'contact', 'contact_id > 0');
		$cleanArray = array();
		$count      = 0;
		if ($contacts != false) {
			foreach ($contacts as $contact) {
				if (!in_array($contact->google_id, $cleanArray)) {
					array_push($cleanArray, $contact->google_id);
				} else {
					$this->db->delete('contact', array('contact_id', $contact->contact_id));
					//echo "removed duplicate entry...<br/>";
					$count++;}
			}

			echo "There are ".count($cleanArray)." useable leads.<br/>";
			echo "PURGE removed $count duplicate contacts.";

		}

		//foreach contact select phone number where phone number is = contact phone number

		//if != false -- delete the contact by id as it is a duplicate

	}

	public function setStatus($contact_id) {
		if ($_POST) {
			$statusSet = $this->db->update('contact', $_POST, array('contact_id', $contact_id));
			if ($statusSet != false) {
				$_SESSION['flashMessage'] = array('success', '', 'Updated Contact!');
			} else {
				$_SESSION['flashMessage'] = array('danger', '', 'Unable to update contact at this time!');
			}

			$this->redirect("contact/info/$contact_id");
		}
	}

	public function restoreAll() {
		$count = 0;
		$trash = $this->db->read('*', 'trash', "trash_id > 0");
		foreach ($trash as $t) {
			//echo $t->table_from_id;
			$this->db->restore($t->table_from, $t->table_from_id);
			$count++;
		}
		echo "Restored $count items.";

	}

}
?>
