<?php
class User extends Controller {

	public function __construct() {
		require ('db.php');
		$this->db = $db;
	}
	function index() {
		require_once ('init.php');
		if (isset($user_id)) {
			$this->redirect('dashboard');//default loggedin view
		} else {
			$this->redirect('user/login');
		}
	}
	function myaccount($this_user_id) {

		require_once ('init.php');
		if (isset($user)) {

			//load the profile page of that user
			$layout = 'profile';

			//start content
			require_once ('application/plugins/grav.php');

			//check if user is following this_user
			if ($this_user_id == $user_id) {
				$pageVar['is_owner'] = "true";
			} else {
				$pageVar['is_owner'] = "false";
			}

			##################### FOLLOWERS SECTION ##############################
			$followers = $this->db->read('user_id', 'follow', "this_user_id = '$this_user_id'");

			if ($followers != false) {
				$followers = get_object_vars($followers[0]);
			} else {
				$followers = array();
			}

			$pageVar['total_followers'] = count($followers);

			if (in_array($user_id, $followers)) {
				$pageVar['is_following'] = "true";
			} else {
				$pageVar['is_following'] = "false";
			}
			######################### END FOLLOWERS SECTION ######################

			######################### FOLLOWING SECTION ##########################
			$following = $this->db->read('user_id', 'follow', "user_id = '$this_user_id'");

			if ($following != false) {
				$followers = get_object_vars($following[0]);
			} else {
				$following = array();
			}
			$pageVar['total_following'] = count($following);

			$this_user = $this->db->read("*", "user", "user_id = '$this_user_id'");
			if ($this_user != false) {

				$this_user       = $this_user[0];
				$grav            = get_gravatar($this_user->email, $s = 300);
				$this_user->grav = $grav;

				//get this users droplets
				$droplets            = $this->db->read("*", "droplet", "user_id = '$this_user_id'");
				$pageVar['droplets'] = $droplets;

				$pageVar['this_user'] = $this_user;

				require_once ('render.php');
			} else {
				$this->redirect('/error/code/404');
			}
		}
	}

	function login() {

		if ($_POST) {
			$email    = $_POST['email'];
			$password = self::dh_hash($_POST['password']);

			//perform encrypting function here

			//read the user table for the provided username
			$user = $this->db->read("*", "user", "email = '$email' ");

			if ($user) {
				$user = $user[0];

				if ($user->password == $password) {
					$_SESSION['user_id'] = $user->user_id;
					//$_SESSION['account_id'] = $user->account_id;
					//$_SESSION['admin']      = $user->admin;

					//update last login2008-07-17T09:24:17Z
					$last_login = date("Y-m-d h:i:s a", time());
					$this->db->update("user", array("last_seen" => $last_login), array("user_id", $user->user_id));

					$this->redirect('/user');
				} else {

					//incorrect password
					$_SESSION["flashMessage"] = array("danger", "", "Incorrect Password.");
					$this->redirect('user/login');
				}
			} else {

				//user doesn't exist
				$_SESSION["flashMessage"] = array("danger", "", "$email is not valid.");
				$this->redirect('/user/login');
			}
		} else {
			$layout       = 'login';
			$page["form"] = "
			<form class='form-signin validate-form' action='/user/login' method='POST'>
            <img src='".LOGO_W."' style='width:200px; vertical-algin:middle;'/>
	        <h2 class='form-signin-heading'>Please Login!</h2>
			<fieldset>
		  	<input id='email' name='email' type='text' class='input-block-level required email' placeholder='Email address'>
		  	<input id='password' name='password' type='password' class='input-block-level required' placeholder='Password'>
		 	<button name='send' class='btn btn-medium btn-primary btn-block' type='submit'>Login</button>
        	</fieldset>
      		</form>

			";
			$pageVar = array('page' => $page);
			require_once ('render.php');

			//init the page

		}
	}

	function logout() {
		session_destroy();
		header('location: /');
	}

	function register() {
		if ($_POST) {
			$_POST['password'] = self::dh_hash($_POST['password']);
			$createUser        = $this->db->create("user", $_POST);

			if ($createUser != false) {
				//send a welcome email here
				$_SESSION['flashMessage'] = array("success", "", "Great! We sent the auth key to your phone.");
				$this->redirect('user/auth');

			}
		} else {
			$layout       = 'login';
			$page["form"] = "
			<form class='form-signin validate-form' action='/user/register' method='POST'>
			<img style='max-width:100%; vertical-algin:middle;'src='".LOGO_W."'>
	        <h2 class='form-signin-heading'>Please Enter Your Mobile Number<span ></span></h2>
			<fieldset>
		  	<input id='phone' name='user_phone' type='text' class='input-block-level required' placeholder='User Phone'>
		 	<button class='btn btn-medium btn-primary btn-block' type='submit'>Register</button>
        	</fieldset>
      		</form>
      		<h4>Already Have an Account? <a href='/user/login'>Sign In</a></h4>
			";
			$pageVar['page'] = $page;
			require_once ('render.php');

			//init the page

		}
	}

	public function resetPassword() {
		if ($_POST) {
			//send email with user password??

		} else {
			$layout       = 'login';
			$page["form"] = "
            <form class='form-signin validate-form' action='/user/resetPassword' method='POST'>
            <img style='max-width:100%; vertical-algin:middle;'src='".LOGO_W."'>
            <h2 class='form-signin-heading'>Enter Your Email<span ></span></h2>
            <fieldset>
              <input id='email' name='email' type='text' class='input-block-level required email' placeholder='Email'>

            <button class='btn btn-medium btn-primary btn-block' type='submit'>Send Reset Email</button>
            </fieldset>
            </form>
            ";
			$pageVar = array('page' => $page);
			require_once ('render.php');
		}
	}

	public function edit() {
		require_once ('init.php');
		if ($_POST) {
			$update = $this->db->update('user', $_POST, array("user_id", $user_id));
			$this->redirect('/user/profile/'.$user_id);

		} else {
			$layout       = 'basic_layout';
			$page["form"] = "
            <form class='form-signin validate-form' action='/user/edit' method='POST'>
            <h2 class='form-signin-heading'>Edit Your Profile<span ></span></h2>

            <fieldset>

              <input id='first_name' name='first_name' type='text' class='input-block-level required' placeholder='First Name' value='".$user[0]->first_name."'>

              <input id='last_name' name='last_name' type='text' class='input-block-level required' placeholder='Last Name' value='".$user[0]->last_name."'>

              <input id='email' name='email' type='text' class='input-block-level' placeholder='Email' value='".$user[0]->email."'>

              <input id='location' name='location' type='text' class='input-block-level required' placeholder='Location' value='".$user[0]->location."'>

            <br/><br/>
            <button class='btn btn-medium btn-primary' type='submit'>Save</button>
            </fieldset>
            </form>
            <hr/>
            <p class='alert alert-info'><i class='fa fa-warning'></i> DropletHub uses Gravatar for profile pictures. <a href='https://gravatar.com'>Set your gravatar up here!</a></p>
            ";
			$pageVar['page'] = $page;
			require_once ('render.php');
		}
	}

	public function sendmessage($this_user_id) {
		require_once ('init.php');
		if ($_POST) {
			$from_name = $user[0]->first_name." ".$user[0]->last_name;
			$this_user = $this->db->read('first_name,last_name', 'user', "user_id = '$this_user_id'");
			$to_name   = $this_user[0]->first_name." ".$this_user[0]->last_name;
			$subject   = $_POST['subject'];
			$message   = $_POST['message'];
			$date_sent = date("D M d Y", time())." at ".date('h:i a', time());

			$create = $this->db->create("message", array("message_to" => $this_user_id, "message_from" => $user_id, "message_from_name" => $from_name, "message_to_name" => $to_name, "subject" => $subject, "message" => $message, "date_sent" => $date_sent));
			if ($create != false) {
				$_SESSION['flashMessage'] = array("success", "", "Message Sent!");
			} else {
				$_SESSION['flashMessage'] = array("danger", "", "Unable to send message!");
			}
			$this->redirect('/user/profile/'.$this_user_id);

		} else {
			$layout     = 'basic_layout';
			$this_user  = $this->db->read("first_name", "user", "user_id = '$this_user_id'");
			$first_name = $this_user[0]->first_name;

			$page["form"] = "
            <form class='form-signin validate-form' action='/user/sendmessage/$this_user_id' method='POST'>
            <h2 class='form-signin-heading'> Send A Message to $first_name<span></span></h2>

            <fieldset>
                <label><p>Subject</p></label>
              <input type='text' name='subject' class='required input-block-level' placeholder='Subject'>
              <label><p>Message</p></label>
              <textarea name='message' id='message' class='required input-block-level' placeholder='Message'></textarea>

            <br/><br/>
            <button class='btn btn-medium btn-danger' type='submit'>Send Message</button>
            </fieldset>
            </form>
";
			$pageVar['page'] = $page;
			require_once ('render.php');
		}
	}

	private function dh_hash($password) {
		$iterations = 256;
		$hash       = $password;

		for ($i = 1; $i <= $iterations; $i++) {
			$hash = md5($hash);
		}

		return $hash;
	}

}
?>