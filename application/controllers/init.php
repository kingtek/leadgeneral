<?php

$uri = explode('/', $_SERVER['REQUEST_URI']);
if (isset($uri[1])) {
	$controller = $uri[1];
}
if (isset($uri[2])) {
	$method = $uri[2];
}

//init is called before any actions are completed in the controller
if (isset($_SESSION['user_id'])) {
	$user_id = $_SESSION['user_id'];
	$user    = $this->db->read("*", "user", "user_id = '$user_id' ");
	$user    = $user[0];
} else {
	$this->redirect('user/login');
}

require_once ('application/plugins/pip_datatables.php');
?>