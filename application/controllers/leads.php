<?php

class Leads extends Controller {
	public function __construct() {
		require ('db.php');

		$this->db           = $db;
		$this->googleSearch = "https://plus.google.com/local/";
	}

	public function index() {
		require_once ('init.php');
		$layout = 'layout_1';

		$pageVar['title'] = 'Leads';

		//10 most recent queries
		//10 most recent contacts accessed
		//10 most recent lists created
		//recent notes
		$body = "<div class='row'>
                <div class='col-lg-8'>
                    <div class='panel panel-default'>
                        <div class='panel-heading'>
                            <i class='fa fa-leaf fa-fw'></i>Recently Accessed Leads

                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div id='morris-area-chart'></div>
                        </div>
                        <!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>
                </div>";

		$pageVar['body'] = $body;

		require_once ('render.php');
	}

	function buildUrl() {
		if ($_POST) {

			$industry = str_replace(" ", "+", (strtolower($_POST['industry'])));

			$location = explode(',', $_POST['location']);
			$city     = urlencode(strtolower($location[0]));
			$state    = urlencode(strtolower($location[1]));

			$_SESSION['city']  = $city;
			$_SESSION['state'] = $state;

		}
		$this->redirect("leads/generate/$industry/$city/$state");
	}

	public function generate($industry, $location_city, $location_state) {
		require_once ('init.php');
		require ('application/plugins/simple_html_dom.php');
		$hash             = md5(time());
		$_SESSION['hash'] = $hash;

		$industry = urlencode(strtolower($industry));

		$location = $location_city." ".$location_state;

		$locationEncoded = rawurlencode($location);

		$parseUrl = $this->googleSearch.$locationEncoded."/s/".$industry;
		//echo "<h2>basic parse of $parseUrl</h2><hr/>";
		$html = file_get_html($parseUrl);

		foreach ($html->find('h3') as $contact) {

			//echo $contact->plaintext;
			foreach ($contact->find('a') as $link) {

				$link = explode('.', $link->href);

				//dan getting google id
				$google_id = str_replace('/about', '', $link['1']);
				$google_id = str_replace('/', '', $google_id);

				$link = "https://plus.google.com".$link['1'];

				//create the contact based off of link
				$contact = $this->db->create("lead", array("google_plus_link" => $link, 'hash' => $hash, 'google_id' => $google_id, 'user_id' => $user_id));

				if ($contact != false) {
					//we have successfully created a tmp record
					$lead_id = $contact['id'];
					//id to refrence update commands

					$html = file_get_html($link);

					//Get the Company Name and make sure it only gets set once.
					foreach ($html->find('div[class=rna]') as $CompanyName) {
						if (!isset($Company)) {

							$Company = $CompanyName->plaintext;
							// echo "<b>Company</b>: <b>" . $Company . "</b>";

							$this->db->update('lead', array("business_name" => $Company), array("lead_id", $lead_id));
						}
					}

					unset($Company);

					//echo $link . "";

					//Checking to see if Verified. If not then it is declared after the foreach loop.
					foreach ($html->find('div[class=xR]') as $verified) {
						//Verified  or Not
						if (!isset($ConfirmVerifiedPage)) {
							$ConfirmVerifiedPage = $verified->plaintext;
							if ($ConfirmVerifiedPage != "") {
								//  echo "<b><font color='006600'>" . $ConfirmVerifiedPage . "</font></b>";
								$isVerified = "YES";
							}

						}
					}

					//If it is not Verified set it here
					if ($ConfirmVerifiedPage == "") {
						//echo "<font color='ff0000'><b>Not Verified</b></font>";
						$isVerified = "NO";
					}
					$this->db->update('lead', array("is_verified" => $isVerified), array("lead_id", $lead_id));
					unset($ConfirmVerifiedPage);

					//Get Address | Number | URL | Category | Hours
					$CompanyInfoCount = 1;
					foreach ($html->find('div[class=Ny]') as $CompanyInfo) {
						if ($CompanyInfoCount == 1) {
							$Address = $CompanyInfo->plaintext;
							//echo "<b>Address</b>: " . $Address . "";
							//$masterArray['business_address'] = $Address;
							$this->db->update('lead', array("business_address" => $Address), array("lead_id", $lead_id));
						}
						if ($CompanyInfoCount == 2) {
							$Number = $CompanyInfo->plaintext;
							//echo "<b>Number</b>: " . $Number . "";
							//$masterArray['business_phone'] = $Number;
							$this->db->update('lead', array("business_phone" => $Number), array("lead_id", $lead_id));
						}
						if ($CompanyInfoCount == 3) {
							$URL = $CompanyInfo->plaintext;
							// echo "<b>URL</b>: " . $URL . "";
							//$masterArray['business_web_url'] = $URL;
							$this->db->update('lead', array("business_web_url" => $URL), array("lead_id", $lead_id));
						}
						if ($CompanyInfoCount == 4) {
							$Category = $CompanyInfo->plaintext;
							//echo "<b>Main Category</b>: " . $Category . "";
							//$masterArray['business_category'] = $Category;
							$Category = str_replace("$", '', $Category);
							$this->db->update('lead', array("business_category" => $Category), array("lead_id", $lead_id));
						}
						if ($CompanyInfoCount == 5) {
							$Hours = $CompanyInfo->plaintext;
							// echo "<b>Hours</b>: " . $Hours . "";
							//$masterArray['business_hours'] = $Hours;
							$this->db->update('lead', array("business_hours" => $Hours), array("lead_id", $lead_id));
						}
						$CompanyInfoCount++;

						//echo $CompanyInfo->plaintext."";

					}

					// echo "<b>Other Categories</b>";

					foreach ($html->find('div[class=s9] span[class=d-s]') as $Categories) {
						$tag                = $Categories->plaintext;
						$_SESSION['tags'][] = $tag;

					}
					//$tags = implode(',', $html->find('div[class=s9] span[class=d-s]'));//comma join these as tags to use later
					//possibly create a tag table, and just refrence tag by id and / or create new tags for more matching options later
					//$masterArray['business_tags'] = $tags;

					//echo " <b>Views</b>";

					//Show Profile Views & Followers. Only need to see the number once.
					//If there are no followers it only shows views. Uses Pipe to separate
					foreach ($html->find('div[class=Zmjtc]') as $CompanyViews) {
						if (!isset($Views)) {
							$Views = $CompanyViews->plaintext;
							if ($Views < 1) {
								$Views = 0;
							}
							str_replace('views', "", $Views);
							$this->db->update('lead', array("business_views" => $Views), array("lead_id", $lead_id));
							unset($Views);

							//echo $Views . "";
						}
					}

					//$masterArray['business_views'] = $Views;
				}
				// echo "<hr align='left' width='50%'>";
			}

			//update record with masterArray
			//$this->db->update('lead', $masterArray, array("lead_id", $lead_id));
		}

		//echo $html->plaintext;

		//Dustin can do the parsing magic from here now that everything is in place
		$this->redirect('contact/searchResult');
	}

}

?>