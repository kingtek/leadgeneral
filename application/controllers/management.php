<?php

class Management extends Controller {
	public function __construct() {
		require ('db.php');

		$this->db = $db;
	}

	public function index() {
		require_once ('init.php');
		$layout = 'layout_1';

		$pageVar['title'] = 'Management';

		//add new user
		//depending on level, add new account

		$body = "<div class='row'>
                <div class='col-lg-8'>
                    <div class='panel panel-default'>
                        <div class='panel-heading'>
                            <i class='fa fa-leaf fa-fw'></i>Management Widget

                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div id='morris-area-chart'></div>
                        </div>
                        <!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div>
                </div>";

		$pageVar['body'] = $body;

		require_once ('render.php');
	}

}

?>