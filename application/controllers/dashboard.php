<?php

class Dashboard extends Controller {
	public function __construct() {
		require ('db.php');

		$this->db = $db;
	}

	public function index() {
		require_once ('init.php');
		$layout = 'layout_1';

		$pageVar['title'] = 'Dashboard';

		//getProcess count
		$process = $this->db->read('process_id', 'process', "status = 'unprocessed' AND user_id = '$user->user_id'");
		if ($process != false) {
			$active_process = count($process);
		} else {
			$active_process = 0;
		}

		$body = "<div class='row'>
                <div class='col-lg-3 col-md-6'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            <div class='row'>
                                <div class='col-xs-3'>
                                    <i class='fa fa-leaf fa-5x'></i>
                                </div>
                                <div class='col-xs-9 text-right'>
                                    <div class='huge'>0</div>
                                    <div>Total Leads</div>
                                </div>
                            </div>
                        </div>
                        <a href='#'>
                            <div class='panel-footer'>
                                <span class='pull-left'>Generate Leads</span>
                                <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                                <div class='clearfix'></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class='col-lg-3 col-md-6'>
                    <div class='panel panel-green'>
                        <div class='panel-heading'>
                            <div class='row'>
                                <div class='col-xs-3'>
                                    <i class='fa fa-spinner fa-spin fa-5x'></i>
                                </div>
                                <div class='col-xs-9 text-right'>
                                    <div class='huge'>$active_process</div>
                                    <div>Active Lead Processes</div>
                                </div>
                            </div>
                        </div>
                        <a href='/process'>
                            <div class='panel-footer'>
                                <span class='pull-left'>View Processes</span>
                                <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                                <div class='clearfix'></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class='col-lg-3 col-md-6'>
                    <div class='panel panel-yellow'>
                        <div class='panel-heading'>
                            <div class='row'>
                                <div class='col-xs-3'>
                                    <i class='fa fa-list-ul fa-5x'></i>
                                </div>
                                <div class='col-xs-9 text-right'>
                                    <div class='huge'>0</div>
                                    <div>Active Lists</div>
                                </div>
                            </div>
                        </div>
                        <a href='#'>
                            <div class='panel-footer'>
                                <span class='pull-left'>View Lists</span>
                                <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                                <div class='clearfix'></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class='col-lg-3 col-md-6'>
                    <div class='panel panel-red'>
                        <div class='panel-heading'>
                            <div class='row'>
                                <div class='col-xs-3'>
                                    <i class='fa fa-fire fa-5x'></i>
                                </div>
                                <div class='col-xs-9 text-right'>
                                    <div class='huge'>0</div>
                                    <div>Hot Leads</div>
                                </div>
                            </div>
                        </div>
                        <a href='#'>
                            <div class='panel-footer'>
                                <span class='pull-left'>View Hot Leads</span>
                                <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                                <div class='clearfix'></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class='panel panel-default'>
                        <div class='panel-heading'>
                            <i class='fa fa-search fa-fw'></i> Recent Queries
                        </div>
            </div>



";

		$pageVar['body'] = $body;

		require_once ('render.php');
	}

}

?>