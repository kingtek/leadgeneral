<?php

/**
 * The DB model is the Database Abstraction tool. ALL databse interaction is controlled via this model.
 *
 */

class Db_model extends Model {

	/**
	 * Creates new record for given object
	 *
	 * <pre>
	 *
	 * $db->create("user",
	 * 					array(
	 * 						"first_name"=>"Dan",
	 * 						"last_name"=>"Sikes"
	 * 							));
	 *
	 * </pre>
	 *
	 * @param string  the table to create a new record in
	 * @param  array  key value array where table.field = key
	 * @return array table and id are returned if successful.
	 * @return bool false is returned if create was not successful.
	 *
	 * @example if successful array("table"=>"user","id"=>"2")
	 * @example if unsuccessful 'false'
	 */
	public function create($table, $params) {
		$id    = $table."_id";
		$table = $this->escapeString($table);

		$result = $this->execute("INSERT INTO `$table` (`$id`) VALUES (NULL);");

		$newId = mysql_insert_id();
		$ref   = array($id, $newId);

		foreach ($params as $key => $value) {

			$set = array($key => $value);
			#echo "$key = $value.<br>";
			$update = self::update($table, $set, $ref);
			if ($update != false) {

			} else {
				#print_r($set)."<br>";
			}

		}

		//do a read, if the data is there, the create was successful.
		$read = self::read($id, $table, "$id = $newId");

		if ($read != false) {
			return array("table" => $table, "id" => $newId);
		} else {
			return false;
		}
	}

	/**
	 *  read data from the database.
	 *
	 * <pre>$db->read("*","user","first_name = 'dan' OR last_name = 'sikes'")</pre>
	 *
	 *  @param string $select The select string.
	 *  @param string $table  The table to query.
	 *  @param string $statement The query statement to execute.
	 *
	 *  @return  Object Returns objects matching query results.
	 *  @return  bool Returns false if nothing matched query results.
	 */

	public function read($select, $table, $statement) {
		$id = $table."_id";
		//die("SELECT $select FROM `$table` WHERE $statement ORDER BY `$table`.`$id` DESC");
		$result      = $this->query("SELECT $select FROM `$table` WHERE $statement");
		$resultCount = count($result);

		if ($resultCount > 0) {

			return $result;

		} else {
			return false;
		}
	}

	/**
	 *  updates record(s) in the database.
	 *
	 *
	 *  <pre>
	 *
	 *  $db->update("user",array(
	 *
	 *      "field"=>"value",
	 *      "field"=>"value",
	 *      "field"=>"value"
	 *
	 *                      ),
	 *                     array("field"=>"value"));
	 *  </pre>
	 *
	 *	**NOTE** If provided field doesn't exist, it is automatically created.
	 * @param string   table to update
	 * @param array   array of field value keypairs
	 * @param array   refrence identifier array
	 * @return  bool returns true if successful
	 * @return  bool returns false if unsuccessful
	 *
	 */

	public function update($table, $set, $refArray) {

		$id          = $table.'_id';
		$columnArray = self::describe($table);

		//fv (field-value) is multiple arrays
		$ref    = $refArray[0];
		$refVal = $refArray[1];

		$error = 0;

		foreach ($set as $field => $value) {

			$value = htmlspecialchars_decode($this->escapeString($value));

			if (!in_array($field, $columnArray)) {

				//create the column in the table

				//get information from value
				if (is_numeric($value)) {

					//set the attr to INT(11)
					$attr = "INT( 11 ) NULL";
				} elseif (strlen($value) > 255) {

					//set the attr to TEXT
					$attr = "TEXT";
				} else {

					//set the attr to VARCHAR(255)
					$attr = "VARCHAR( 255 ) NULL";
				}

				$alter = $this->execute("ALTER TABLE `$table` ADD `$field` $attr");
			}
			#echo("UPDATE `$table` SET  `$field` =  '$value' WHERE  `$table`.`$ref` = '$refVal';")."<br>";

			$update = $this->execute("UPDATE `$table` SET  `$field` = '$value' WHERE  `$table`.`$ref` = '$refVal';");

			//need to check for each update to make sure it succedded
			$check = self::read($id, $table, "`$table`.`$field` = '$value' ");

			if ($check != false) {

				//success

			} else {

				//error updating field
				$error++;
				//echo "Error with $field . $value <br>";

			}
		}

		if ($error == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *  deletes record(s) in the database. Also creates a backup of deleted record.
	 *  <pre>$db->delete("user",array("user_id","1"));</pre>
	 *
	 *
	 * @param string   table to delete from
	 * @param array   refrence identifier string
	 * @return  bool returns true if successful
	 * @return  bool returns false if unsuccessful
	 */

	public function delete($table, $refArray) {
		$id     = $table.'_id';
		$table  = $this->escapeString($table);
		$ref    = $refArray[0];
		$refVal = $refArray[1];

		$data   = self::read("*", $table, "$ref = '$refVal' ");
		$dataId = $data[0]->$id;

		$sdata = serialize($data);

		//create the delete record
		$createDelete = self::create("trash", array("table_from" => $table, "record_data" => $sdata, "table_from_id" => $dataId));

		if ($createDelete == true) {

			//params need to be defined
			//echo "DELETE FROM `$table` WHERE `$table`.`$ref` = '$refVal';";

			$result = $this->execute("DELETE FROM `$table` WHERE `$table`.`$ref` = '$refVal';");

		} else {
			return false;
		}

		//attempt to read data, if its not there, delete was a success
		$read = self::read("*", $table, "$ref = '$refVal'");

		if ($read == false) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *  displays available fields for given table.
	 *
	 * @param string   table to describe
	 * @param array   (optional) excludes given array items if they exist
	 * @return  array returns fields in table
	 * @return  bool returns false if unsuccessful
	 */

	public function describe($table, $protected = array()) {

		$result = $this->query("DESCRIBE $table");

		if (count($result) > 0) {
			foreach ($result as $r) {
				$columnArray[] = $r->Field;
			}
			if (isset($protected)) {
				foreach ($columnArray as $ca) {
					if (!in_array($ca, $protected)) {
						$description[] = $ca;
					}
				}
			} else {
				$description = $columnArray;
			}

			return $description;
		} else {
			return 0;
		}
	}

	/**
	 *  restores deleted data to its exact value before it was deleted.
	 *
	 *  <pre>$db->restore("user",1);</pre>
	 *
	 * @param string   table to restore to
	 * @param int   id of deleted item
	 * @return  bool returns true if successful
	 * @return  bool returns false if unsuccessful
	 */

	public function restore($table, $id) {

		$data = self::read("*", "trash", "table_from = '$table' AND table_from_id = '$id' ");
		if ($data != false) {

			//convert the serialized record data to an array
			foreach ($data as $d) {
				$dataArray = unserialize($d->record_data);
				$dataArray = $dataArray[0];
				$dataArray = get_object_vars($dataArray);
				$tableId   = $table."_id";

				unset($dataArray[$tableId]);
				#echo "<pre>";
				#print_r($dataArray);
				#echo "</pre>";

				#print_r($dataArray);
				$create = self::create($table, $dataArray);
				if ($create != false) {
					$newId = $create['id'];
					self::update($table, array($tableId => $id), array($tableId, $newId));
					$this->execute("DELETE FROM `trash` WHERE `table_from` = '$table' AND `table_from_id` = '$id' ");
				}

			}

		} else {
			die("Unable to read trash table.");
		}
	}
/**
 *  Returns an array of tables of the database.
 */

	private function showTables() {
		$str = 'Tables_in_'.DB;

		$data = $this->query("SHOW tables");
		if (count($data) > 0) {
			foreach ($data as $item) {
				$value[] = $item->$str;
			}
			if (count($value) > 0) {
				return $value;
			} else {
				return false;
			}

		} else {
			return false;
		}

	}

/**
 * An extremely helpful database refrence tool
 *
 * Use this tool to display a preformatted list of existing tables with their respective field names
 *
 *
 */

	public function sniff() {
		$tables = self::showTables();

		foreach ($tables as $t) {

			echo "<pre>";
			echo "<h3>$t</h3>";
			print_r(self::describe($t));
			echo "</pre>";
		}
	}

}
?>
