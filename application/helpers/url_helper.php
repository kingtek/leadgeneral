<?php

class Url_helper {

	function base_url()
	{
		global $config;
		return $config['base_url'];
	}

	function stat()
	{
		global $config;
		return $config['stat'];
	}

	function logo_w()
	{
		global $config;
		return $config['logo_w'];
	}

	function logo_h()
	{
		global $config;
		return $config['logo_h'];
	}



	
	function segment($seg)
	{
		if(!is_int($seg)) return false;
		
		$parts = explode('/', $_SERVER['REQUEST_URI']);
	    return isset($parts[$seg]) ? $parts[$seg] : false;
	}
	
}

?>