<?php
if(isset($_SESSION["flashMessage"])){
$flashMessage = $_SESSION["flashMessage"];
	if(!empty($flashMessage)){
			/*
			====Flash Messages==== 
			$flashMessage is an array that will display attractive messages to the view.
				//first paramater of the array is the type of message that it is
				###PARAM 1 OPTIONS####
				s = Success
				i = Information
				w = Warning
				d = Danger

				//the second paramter is the font awesome icon refrence to be used
				---> see docs @ https://fortawesome.github.io/Font-Awesome/icons/
				NOTE: You don't need to prepend the fa, it is done automatically, so just use the icon name
				ie 'fa fa-warning' == 'warning'

				//the third paramter of the array is the message it self

				//COMPLETED EXAMPLE OF A FLASH MESSAGE
				$flashMessage = array("success","thumbs-up","Success Message Here!");

			*/
			echo "<div class='alert alert-".$flashMessage[0]." alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fa fa-".$flashMessage[1]."'></i>  ".$flashMessage[2]."</div>";
}
}
unset($_SESSION['flashMessage']);

?>