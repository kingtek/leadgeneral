
<?php
require_once ('header.php');
$city  = $_SESSION['city'];
$state = $_SESSION['state'];
//require_once ('sidebar.php');
?>
<div class='container'>
<div class="col-md-12">
<h3>Results</h3>
<hr/>
<?php $tbl->printTable();?>
<hr/>
<?php
if ($_SESSION['tags']) {
	?>
				<h4>Based On Recent Searches You Might Also Try</h4>
			<h5>Searching near <?=ucfirst($city).",".ucwords($state);
	?>&nbsp;
			&nbsp;
			 <a href='/contact/clearSearchHistory'>Clear Search History</a></h5>
			<ul>
	<?php
	$tags = array_unique($_SESSION['tags']);

	foreach ($tags as $tag) {
		$industry = rtrim(str_replace(" ", "+", (strtolower($tag))), '+');
		echo "<li><a href='/contact/generateLeads/$industry/$city/$state'>$tag</a></li>";
	}
	echo "</ul>";
} else {
	echo "As you search suggestions will appear here.";
}
?>
<!-- /Main -->
</ul>
</div>
<?php
require_once ('footer.php');
?>
