<link href="<?=STAT;?>css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
body {
padding-top: 40px;
padding-bottom: 40px;
-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100,FinishOpacity=78,Style=2)";
filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=100,FinishOpacity=78,Style=2);}

 .form-signin {
 max-width: 500px;
 padding: 130px 29px 29px;
margin: 0 auto 20px;
background-color: transparent;
border: 0px solid #e5e5e5;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
-moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
 box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
.form-signin .form-signin-heading{color:#000;margin-bottom: 25px;font-size: 23px;font-weight: 400;margin-bottom: 10px;}
.form-signin a{color: black;font-weight: 400;font-size: 10px;}
.form-signin input[type="text"],
.form-signin input[type="password"] {font-size: 16px;
height: auto;
margin-bottom: 15px;
padding: 7px 9px;
opacity: 0.9;
border: 1px solid #28759B;}

.error{color:red;}

</style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body class="animated fadeIn">

    <div class="container">

<center>

<?php
require_once ('application/helpers/flashMessage.php');
?>


<?=$page["form"];?>

</center>
    </div> <!-- /container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(function(){
    $('.validate-form').validate();
  })
</script>