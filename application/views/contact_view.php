<?php
require_once ('header.php');
echo "<hr/>";
require_once ('application/helpers/flashMessage.php');
?>
<div class="col-md-12">
<?php
echo $page["form"];
?></div>
<!-- /Main -->
</div>
<div class="col-md-12">
<h3>Notes <a onclick="displayNoteForm()" class='btn btn-sm btn-default'><i class='fa fa-file-o'></i> Add Note</a> </h3>
<div id='noteForm'>
	<form action="/contact/saveNote/<?=$contact_id;?>" method="post" />
	<textarea class='form-control' name='message'></textarea><hr/>
	<input type='submit' value='Save' class='btn btn-sm btn-default'  style="float:right;" />
	</div>
	<br/><hr/>
<?php

echo $page["note"];?>
<?php
require_once ('footer.php');
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
  	$("#noteForm").hide();
} );

function displayNoteForm(){
	$("#noteForm").slideToggle();
}
</script>