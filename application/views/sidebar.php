<body style='z-index:-1000px;'>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=BASE_URL;?>"><img src='<?=LOGO_W;?>' style='max-width:225px; vertical-align:middle; position: absolute; top:-10px;' /></a>
            </div>
            <!-- /.navbar-header -->


            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i><?=$user->name;?><i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="/user/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <form action='/process/create'class="navbar-form navbar-right" role="search" method='post'>
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Industry" name='industry'>
  </div>
  <div class="form-group">
    <input id='location' type="text" class="form-control" placeholder="City" name='location'>
    <input type="hidden" class="form-control" name='process_type' value='lead'>
    <input type="hidden" class="form-control" name='user_id' value='<?=$user->user_id;?>'>
  </div>
  <button type="submit" class="btn btn-default">Generate</button>
</form>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="/lists"><i class="fa fa-list-ul fa-fw"></i> Lists</a>
                        </li>

                         <li>
                            <a href="/management"><i class="fa fa-eye fa-fw"></i> Management</a>
                        </li>

                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

