<?php
require_once ('header.php');
require_once ('sidebar.php');
?>
<!-- Page Content -->
        <div id="page-wrapper">
<?php require_once ('application/helpers/flashMessage.php');?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?=$title;?></h1>
                    <div>
<?=$body;?>
</div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php
require_once ('footer.php');
?>