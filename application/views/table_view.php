
<?php
require_once ('header.php');

//require_once ('sidebar.php');
?>
<div class="col-md-12">

		<table id='example' class='display' cellspacing='0' width='100%'>
        <thead>
            <tr>
                <th>Business Name</th>
                <th>Website</th>
                <th>Industry</th>
                <th>Address</th>
            </tr>
        </thead>

<?php
foreach ($contacts as $contact) {
	$contact_id = $contact->tmp_contact_id;

	$business_name    = "<a href='/contact/info/$contact_id'>" .$contact->business_name."</a>";
	$business_phone   = "<a href='/contact/info/$contact_id'>" .$contact->business_phone."</a>";
	$business_web_url = "<a href='/contact/info/$contact_id'>" .$contact->business_web_url."</a>";
	//$business_email    = "<a href='/contact/info/$contact_id'>" .$contact->business_email."</a>";
	$business_industry = "<a href='/contact/info/$contact_id'>" .$contact->business_category."</a>";
	$business_address  = "<a href='/contact/info/$contact_id'>" .$contact->business_address."</a>";
	//$business_state    = "<a href='/contact/info/$contact_id'>" .$contact->business_state."</a>";

	echo "<tr><td>$business_name</td><td>$business_web_url</td><td>$business_industry</td><td>$business_address</td></tr>";
}

echo "<tbody></tbody></table>";
echo "<h3>You Might Also Try</h3>";
echo "<ul>";
$tags = array_unique($_SESSION['tags']);

$city  = $_SESSION['city'];
$state = $_SESSION['state'];

foreach ($tags as $tag) {
	$industry = rtrim(str_replace(" ", "+", (strtolower($tag))), '+');
	echo "<li><a href='/contact/generateLeads/$industry/$city/$state'>$tag</a></li>";
}
echo "</ul>";
?>
<!-- /Main -->
<?php
require_once ('footer.php');
?>
<script>
  $(document).ready(function() {
    $('#example').dataTable();
} );
</script>